package org.example.repositories;

import org.example.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonenRepository extends JpaRepository<Person, Long> {
}