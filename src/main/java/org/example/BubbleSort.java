package org.example;

public class BubbleSort {
    public static void bubbleSort(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    // Tausche arr[j] und arr[j+1]
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    public static class BubbleSortGeneric<T extends Comparable<T>> {
        public void bubbleSortGen(T[] arr) {
            int n = arr.length;
            for (int i = 0; i < n - 1; i++) {
                for (int j = 0; j < n - i - 1; j++) {
                    if (arr[j].compareTo(arr[j + 1]) > 0) {
                        // Tausche arr[j] und arr[j+1]
                        T temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
                }
            }
        }


        public static void main(String[] args) {

            int[] arr = {64, 34, 25, 12, 22, 11, 90};
            System.out.println("Unsortiertes Array:");
            printArray(arr);

            bubbleSort(arr);

            System.out.println("Sortiertes Array:");
            printArray(arr);

            Integer[] intArr = {64, 34, 25, 12, 22, 11, 90};
            BubbleSortGeneric<Integer> intBubbleSort = new BubbleSortGeneric<>();
            System.out.println("Unsortiertes Integer-Array:");
            intBubbleSort.printArray(intArr);
            intBubbleSort.bubbleSortGen(intArr);
            System.out.println("Sortiertes Integer-Array:");
            intBubbleSort.printArray(intArr);

            String[] strArr = {"apple", "orange", "banana", "grape", "cherry"};
            BubbleSortGeneric<String> strBubbleSort = new BubbleSortGeneric<>();
            System.out.println("Unsortiertes String-Array:");
            strBubbleSort.printArray(strArr);
            strBubbleSort.bubbleSortGen(strArr);
            System.out.println("Sortiertes String-Array:");
            strBubbleSort.printArray(strArr);

        }

        public static void printArray(int[] arr) {
            for (int i = 0; i < arr.length; i++) {
                System.out.print(arr[i] + " ");
            }
            System.out.println();
        }

        public void printArray(T[] arr) {
            for (T element : arr) {
                System.out.print(element + " ");
            }
            System.out.println();
        }
    }
}

