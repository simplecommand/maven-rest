package org.example.controller;

import org.example.entities.Person;
import org.example.repositories.PersonenRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.BDDMockito.*;
@WebMvcTest(PersonController.class)
public class PersonControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PersonenRepository personRepository;

    @Test
    @WithMockUser(username = "user", password = "password", roles = "USER")
    public void testAddPerson() throws Exception {
        Person person = new Person();
        person.setName("Mustermann");
        person.setVorname("Max");
        // Setze weitere Felder...

        given(personRepository.save(any(Person.class))).willReturn(person);

        mockMvc.perform(post("/person")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"Mustermann\",\"vorname\":\"Max\",\"plz\":\"12345\",\"ort\":\"Musterstadt\",\"strasse\":\"Musterstraße 1\",\"geburtstag\":\"1980-01-01\"}")
                        .with(httpBasic("user", "password")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Mustermann"));
    }

    @Test
    @WithMockUser(username = "user", password = "password", roles = "USER")
    public void testGetAllPersons() throws Exception {
        Person person = new Person();
        person.setName("Mustermann");
        // Setze weitere Felder...

        List<Person> allPersons = Arrays.asList(person);
        given(personRepository.findAll()).willReturn(allPersons);

        mockMvc.perform(get("/persons")
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(httpBasic("user", "password")))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("Mustermann"));
    }
}